import React from 'react';
import { TextInput,StyleSheet,Text,View,ScrollView}  from 'react-native';
import Container from '../component/Container';
import CustomButton from '../component/CustomButton';
import {Formik}  from 'formik'
import {Vadidators} from '../utills/Vadidators'
import {widthPercentageToDP as wp ,heightPercentageToDP as hp,
listenOrientationChange,removeOrientationListener} from 'react-native-responsive-screen'
import {utils} from '../utills/utils'
import If from '../component/If'
import CustomText from '../component/CustomText'
import { lComponentStyle, pComponentStyle, Typography } from '../style/Global';

    interface Props{
        }
    interface State{
      form:{
        emailInuputValue : string,
        passwordInuputValue : string,
      };
      orientation : string
    }
    
 class Login extends React.Component<Props,State>{
      private passwordInputref;
        constructor(props) {
          super(props);
            this.state = {
              form:{
              emailInuputValue:"",
              passwordInuputValue:"",
              },
              orientation:"portrait"
            }
        }
      componentDidMount(){
        listenOrientationChange(this)
      }
      componentWillUnmount(){
        removeOrientationListener();
      }
    render() {
      return (
        <Container ContainerStyle={{alignItems:"center"}}>  
      
          <CustomText 
            style={[Typography.title,{letterSpacing:4}]}>
            Login
          </CustomText>
          <Formik 
              validationSchema={Vadidators.logoinVadidators}
              initialValues={this.state.form} 
              validateOnMount={true}
              validateOnChange={true}
              onSubmit={()=>{console.log("on submit")}}
            >
            {(props)=>{
            return(
              <View style={{alignItems:"center"}}>
                <TextInput 
                  onSubmitEditing={() => this.passwordInputref.focus()}
                  returnKeyType={"next"}  
                  style={
                    utils.dynamStyle(
                      pComponentStyle.textinput,
                      lComponentStyle.textinput,
                      this.state.orientation)
                    } 
                  onBlur={()=>props.setFieldTouched("emailInuputValue")}
                  onChangeText={props.handleChange("emailInuputValue")}
                  placeholder="email"
                  value={props.values.emailInuputValue}
                />
                <If show={props.dirty && props.touched.emailInuputValue}>
                <CustomText style={[Typography.errorText]}>
                    {props.errors.emailInuputValue}
                  </CustomText> 
                </If>
                <TextInput 
                  onSubmitEditing={()=>{
                  if(props.isValid){
                    console.log("is valid");
                  }else{
                    console.log("from is not vaid")
                  }
                  }}
                  ref={ref => this.passwordInputref = ref} 
                  returnKeyType={"done"}
                  style={
                    utils.dynamStyle(
                    pComponentStyle.textinput,
                    lComponentStyle.textinput,
                    this.state.orientation)
                  } 
                  onBlur={()=>props.setFieldTouched("passwordInuputValue")}
                  onChangeText={props.handleChange("passwordInuputValue")}
                  placeholder="password" 
                  value={props.values.passwordInuputValue}
                /> 
                  <If show={props.dirty && props.touched.passwordInuputValue}>
                    <CustomText style={[Typography.errorText]}>
                      {props.errors.passwordInuputValue}
                    </CustomText> 
                  </If>
                
                <CustomButton 
                useIcon={true}  IconsName={'logo-instagram'}
                  disabled={!props.isValid}
                    title={"LOGO"} 
                      onPress={()=>{
                        if(props.isValid){
                          return props.handleSubmit();
                        }else{
                          console.log("from is not vaid")
                        }
                      }}
                />
              </View>
              
                )
            }}
          </Formik>
       
        </Container>
        );
      }
    }
    const portraiStyle = ()=>{
     return  StyleSheet.create({
      })
    }
const landScapeStyle = ()=>{
  return StyleSheet.create({
  })
}

export default Login;