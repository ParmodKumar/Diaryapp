import * as React from 'react';
import Container from '../component/Container';
import {widthPercentageToDP as wp ,heightPercentageToDP as hp,
    } from 'react-native-responsive-screen'
import DiaryList from '../component/DiaryList';
import { Text,ToastAndroid,View} from 'react-native'
import CustomText from '../component/CustomText';
import { ScrollView } from 'react-native-gesture-handler';
import {Spaning} from '../style/Global'
import If from '../component/If';
import { FloatingAction } from "react-native-floating-action";
import Icon from '../component/Icon'
import  { ReactNativeModal } from 'react-native-modal';
import CreateDiaryItem from '../component/Modal/CreateDiaryItem';
import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet'
import { Daryitem } from '../model/Daryitem';
import { ScreenName } from '../utills/navigation/Routes';
import Reactotron from 'reactotron-react-native';

  interface Props{ 
      route:any,
      navigation:any
  }
  interface State{
      diaryItems:Daryitem[],
      isModalVisible:boolean,
      selectitem:Daryitem,
  }

  export enum ActionType{
    View= 0,
    Edit= 1,
    Delete= 2, 
    cancel= 3
  }
  class Dashboard extends React.Component<Props,State>{ 
    private actionSheetRef: any;
      constructor(props){
          super(props);
            this.state = {
                diaryItems: [ 
                    {
                  id:0,
                  subject: 'my subject 11',
                  date : "Jun:1:2021",
                  Time : "11.23Pm",
                  description:
                    'Today was one of my best day of life.Today was one of my best day of life. Today was one of my best day of life.  ',
                  },
                  {
                    id:1,
                    subject: 'my subject 2',
                    date : "Jun:1:2021",
                    Time : "11.23Pm",
                    description:
                      'Today was one of my best day of life.Today was one of my best day of life. Today was one of my best day of life.  ',
                  },
          ],
          isModalVisible:false,
          selectitem:null
        }; 
      }
      actions = [
          {
            text: 'Add',
            name: 'add',
            icon : <Icon name={'add'}/>,
          },   
        ];
      additem = val =>{
        const diaryItems = this.state.diaryItems;
          diaryItems.push(val)
          this.setState({
            diaryItems,
            isModalVisible:false,
          })
      }
      updateItem = val=>{
        const filteritem = this.deleteandgetitem(val)
        this.setState({diaryItems:filteritem,isModalVisible:false,selectitem:null})
        filteritem.push(val);
      }
      handleActionButtom = index => {
        // if(index == ActionType.cancel){
        //   this.setState({selectitem:null})
        // }else if(index === 1){
        //   this.setState({isModalVisible:true})
        // }
        switch (index) {
          case ActionType.cancel:{
            this.setState({selectitem:null})
            return ToastAndroid.show('cancel @@@@@',ToastAndroid.LONG)
            break;
          }
          case ActionType.Edit:{
            this.setState({isModalVisible:true})

            break;
          }
          case ActionType.View:{
            this.onView();
            break;
          }
          case ActionType.Delete:{
            this.onDelele();
          }
        }
      }
      onView(){
        this.props.navigation.navigate
        (ScreenName.VIEWDARIYITEM,{diaryItem:this.state.selectitem});
        this.setState({selectitem:null})
      }
      onDelele(){
        const filteritem = this.deleteandgetitem(this.state.selectitem)
        this.setState({diaryItems:filteritem})
        return ToastAndroid.show('Deleleitems @@@@@',ToastAndroid.LONG)
      }
      deleteandgetitem(item) :Daryitem[]{
        const diaryItems = this.state.diaryItems;
        const filteritem = diaryItems.filter
        (data=>data.id !== item.id);
        return filteritem
      }
  render(){
    Reactotron.log('hello debuing',this.state.diaryItems);
      return(
          <Container ContainerStyle={{justifyContent:"flex-start",}}>
              <If show={this.state.diaryItems.length > 0 }>   
                  <DiaryList onPress={(val)=>{
                    this.setState({selectitem:val})
                    this.actionSheetRef.show()
                  }} diaryItems={this.state.diaryItems}/>
              </If>
              <If show={this.state.diaryItems.length === 0}>
                <CustomText style={{width: wp('90%'),textAlign:"center"}}>
                    Currenty , No Item is added.Please start adding Ttem 
                    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                </CustomText>
              </If>   
              <FloatingAction
                actions={this.actions}
                onPressItem={name => {
                this.setState({isModalVisible:true})
                }}
              />
              <ActionSheet
                ref={ref => this.actionSheetRef = ref}
                title={<CustomText style={{color: '#000', fontSize: 18}}>Which one do you like?</CustomText>}
                options={['View', 'Edit','Delete', 'cancel']}
                cancelButtonIndex={3}
                destructiveButtonIndex={2}
                onPress={this.handleActionButtom}
              />
              <CreateDiaryItem onUpdate={this.updateItem} diaryItem={this.state.selectitem} onSave={this.additem} isVisible={this.state.isModalVisible}
                onDismiss={()=>this.setState({isModalVisible:false,selectitem:null})}
              />
          </Container>
        ) 
      }
      // componentDidMount(){
      //     console.log(this.props.navigation,this.props.route,)
      // }
      }
  export default Dashboard

