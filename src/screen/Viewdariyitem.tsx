import * as React from 'react';
import { View } from 'react-native';
import Container from '../component/Container';
import CustomText from '../component/CustomText';

interface Props{
    route:any
};
interface Stats{};


class Viewdariyitem extends React.Component<Props,Stats>{
    constructor(props){
        super(props);
        this.state = {}
    }
    render(){
        return(
           <Container ContainerStyle={{justifyContent:"flex-start"}}>
               <CustomText>{this.props.route.params.diaryItem.subject}</CustomText>
               <CustomText>{this.props.route.params.diaryItem.description}</CustomText>
           </Container>
        )
    }
}


export default Viewdariyitem;