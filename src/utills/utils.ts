import * as React from 'react';
import { ImageStyle, TextStyle, ViewStyle ,Platform} from 'react-native';

    export class utils{
        static dynamStyle(
            portraiStyle: 
            ViewStyle
        |   ViewStyle[]
        |   TextStyle
        |   TextStyle[]
        |   ImageStyle
        |   ImageStyle[],
            landScapeStyle:
            ViewStyle 
        |   ViewStyle[] 
        |   TextStyle 
        |   TextStyle[]
        |   ImageStyle 
        |   ImageStyle[],
            orientation:string 
            ){
        return   orientation === "portrait" ? portraiStyle:landScapeStyle
    }
        static IsIos(){
            return Platform.OS === 'ios';
        }
        static image = {
            Daryimg : require('../assets/backgroundimg.jpg')
        }
}