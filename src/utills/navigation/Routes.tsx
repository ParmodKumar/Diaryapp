import { createStackNavigator, } from '@react-navigation/stack';
import * as React from 'react'
import Dashboard from '../../screen/Dashboard';
import Login from '../../screen/Login';
import { PrimaryTheme } from '../../style/Theme';
import {widthPercentageToDP as wp ,heightPercentageToDP as hp,
   } from 'react-native-responsive-screen'
import Viewdariyitem from '../../screen/Viewdariyitem';
import { View } from 'react-native';
import Icon from '../../component/Icon';

const Stack = createStackNavigator();

export enum ScreenName{
    LOGIN = 'Login',
    Dashboard ='dashboard',
    VIEWDARIYITEM = 'Viewdariyitem',
}
export const authStack = ()=>{
return(
    <Stack.Navigator initialRouteName={ScreenName.LOGIN}>
   <Stack.Screen  name={ScreenName.LOGIN} component={Login}/>
  </Stack.Navigator>
)
}

export const aapStack = () => {
    return(
        <Stack.Navigator  initialRouteName={ScreenName.Dashboard}
            screenOptions={{
                title:'My Diary',
                headerTintColor:"#fff",
                // headerLeft:()=>(
                //  <View style={{marginLeft:10,}}>
                //    <Icon name={'menu'} size={30} color={"#fff"}/>
                //  </View>
                // ),
            //     headerRight:()=>(
            //      <View style={{marginLeft:10}}>
            //      <Icon name={'home'} size={30}/>
            //    </View>
            //     ),
            headerStyle:{
                backgroundColor:PrimaryTheme.$Red_COLOR,        
            },
            //   headerTitleStyle: { alignSelf: 'center' },
            }}
        >
        <Stack.Screen  name={ScreenName.Dashboard} component={Dashboard}/>
            <Stack.Screen 
            options={{title:"View",
            headerTintColor:"#fff",
            // headerLeft:()=>(
            //          <View style={{marginLeft:10,}}>
            //            <Icon name={'home'} size={30} color={"#fff"}/>
            //          </View>
            //         ),
        }}
            name={ScreenName.VIEWDARIYITEM} component={Viewdariyitem}/>
        
       </Stack.Navigator>
    )
}  
