import React from 'react';
import { View,Image,StyleSheet ,TouchableOpacity} from 'react-native';
import CustomText from './CustomText';
import {widthPercentageToDP as wp ,heightPercentageToDP as hp,
  } from 'react-native-responsive-screen'
import { utils } from '../utills/utils';
import {Spaning} from '../style/Global'
import { Daryitem } from '../model/Daryitem';

  export interface Props{
    diaryItem:Daryitem;
    onPress:any;
}

const Diaryitem = (props:Props)=>{
    return (
      <TouchableOpacity  onPress={props.onPress}
        activeOpacity={0.3}
         style={{marginBottom:Spaning.small.marginBottom,padding:15}}>
          <View style={{width: wp("70%"),
              marginLeft:Spaning.extrlarg.marginLeft,
              marginBottom:Spaning.extrlarg.marginBottom}}>
              <CustomText 
                style={styles.dataText}>{props.diaryItem.date}
              </CustomText>
              <CustomText 
                style={styles.dataText}>{props.diaryItem.Time}
              </CustomText>
              <View style={{flexDirection:"row"}}>
                  <View>
                    <CustomText 
                      style={{color:"black"}}>{props.diaryItem.subject}
                    </CustomText>
                    <CustomText 
                      style={{color:"black"}}>{props.diaryItem.description}
                    </CustomText>
                  </View>
                  <View>
                     <Image 
                        style={{height: hp('16%'),width: wp("20%"),}} source={utils.image.Daryimg}
                     />
                  </View>
              </View>
          </View>
      </TouchableOpacity>
    )
  }
Diaryitem.defaultProps = {};

const styles = StyleSheet.create(({
  dataText:{
      color:"red"
  }
}))
export default Diaryitem;