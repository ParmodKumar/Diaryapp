import * as React from 'react';
import { View,TextInput ,StyleSheet,ToastAndroid} from 'react-native';
import  { ReactNativeModal } from 'react-native-modal';
import {widthPercentageToDP as wp ,heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { FloatingAction } from "react-native-floating-action";
import Icon from '../Icon';
import { Daryitem } from '../../model/Daryitem';

export interface Props{
    onDismiss:any,
    isVisible:boolean,
    onSave:any,
    diaryItem?:Daryitem,
    onUpdate:any
}
export interface State{
}

const CreateorEditDiaryItem = (props:Props)=>{
 const [subjectTextInput,updatesubjectTextInput] = React.useState('');
 const [descriptionTextInput,updateDescriptionTextInput] = React.useState('');

   const actions = [
        {
          text: props.diaryItem ? "update" : 'Add', 
          name: props.diaryItem ? "update" : 'add',
          icon : <Icon name={'checkmark'}/>,
          position: 1,
        },   
      ];

      React.useEffect(()=>{
        if(props.diaryItem){
          updateDescriptionTextInput(props.diaryItem.description);
          updatesubjectTextInput(props.diaryItem.subject)
        }else{
          updatesubjectTextInput('');
          updateDescriptionTextInput('');
        }
      },[props.diaryItem])
      const onSaveorupdate = ()=>{
         if(subjectTextInput === '' && descriptionTextInput === ''){
         return ToastAndroid.show('Subject And Description is Needed',ToastAndroid.LONG)
        }
        const date = {
          id: props.diaryItem ? props.diaryItem.id :Math.random(),
          subject:subjectTextInput,
          description:descriptionTextInput  
      };
      if(props.diaryItem){
       return props.onUpdate(date);
       
       
      }
        return props.onSave(date);
      }
    return(
      
        <ReactNativeModal style={{backgroundColor:"#fff",margin:0}}
          onDismiss={props.onDismiss}
          onBackButtonPress={props.onDismiss} 
          isVisible={props.isVisible} 
        >
          <View style={{flex: 1}}>  
            <TextInput allowFontScaling={false} value={subjectTextInput} 
              onChangeText={val => updatesubjectTextInput(val)}        
              style={[style.TextInput,{borderBottomWidth:3}]} placeholder={'Enter Subject'}
               />
            <TextInput  allowFontScaling={false} value={descriptionTextInput}
              onChangeText={val => updateDescriptionTextInput(val)}  
              multiline={true} scrollEnabled={true} 
              style={style.TextInput} placeholder={'Start Wright Your Drily'} 
            />
          </View>
          <FloatingAction
            actions={actions}
            onPressItem={onSaveorupdate}
          />
        </ReactNativeModal>
    ) 
}
   
const style = StyleSheet.create({
    TextInput:{
        width: wp("100%"),
    }
})

export default CreateorEditDiaryItem;