import * as React from 'react';
import { TouchableOpacity,Text, ViewStyle,TextStyle,StyleSheet } from 'react-native';
import { PrimaryTheme } from '../style/Theme';
import Icon from './Icon'
import CustomText from './CustomText'
import If from './If';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
    interface Props{
        onPress :any,
        title : string,
        disabled? :boolean,
        buttonstyle? : ViewStyle | ViewStyle[],
        textstyle? : any,
        useIcon? : boolean,
        IconsName?:string,
        IconsSize?:number,
        IconsColor?:string
    }
    const CustomButton = (props:Props) =>{
        return(
            <TouchableOpacity 
                onPress={props.onPress}
                disabled={props.disabled}
                style={[props.disabled ?
                 {...style.buttonstyle,
                 backgroundColor:PrimaryTheme.$DIVIDER_COLOR,
                }:
                 style.buttonstyle,props.buttonstyle ]}
            >
               <If show={props.useIcon}>
               <Icon 
               name={props.IconsName} 
               size={props.IconsSize}
                color={props.IconsColor}
                />
               </If>
              
                <CustomText style={[
                    style.textstyle,props.textstyle]}>
                    {props.title}
                </CustomText> 
            </TouchableOpacity>
        )

        }
        CustomButton.defaultProps = {
            disabled:false,
            useIcon:false,
            IconsSize:30,
            IconsColor:"white"
        }
        const style = StyleSheet.create({
            buttonstyle:{
                height: hp('5%'),
                width: wp('30%'),
                backgroundColor: PrimaryTheme.$BUTTON_COLOR,
                borderRadius:8,
                flexDirection:"row",
                justifyContent:"center",
                alignItems:"center"
            },
            textstyle:{
                color:"#fff",
                textAlign:"center",
                padding:9,
                letterSpacing:2
            }
        })
export default CustomButton;