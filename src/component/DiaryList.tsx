import * as React from 'react'
import { View ,FlatList} from 'react-native';
import { Daryitem as  daryitem} from '../model/Daryitem';
import { Spaning } from '../style/Global';
import Diaryitem from './DiaryItem';

export interface Props{
   diaryItems:daryitem[];
   onPress:any;

}

const DiaryList = (props:Props)=>{
    return ( 
      <FlatList 
        style={{marginTop:Spaning.extrlarg.marginTop}} 
        showsVerticalScrollIndicator={false}
        keyExtractor={(item,index) => index.toString()}
        data={props.diaryItems}
        renderItem={data=>{
          return <Diaryitem onPress={()=>props.onPress(data.item)} diaryItem={data.item}/>
        }}
      />
    )
}

export default DiaryList;

// {props.diaryItems.map((diaryItem,index)=>{
//     return <Diaryitem  diaryItem={diaryItem} index={index}/>
// })}