import * as React from 'react';
import {SafeAreaView,StyleSheet, ViewStyle} from 'react-native';
import { PrimaryTheme } from '../style/Theme';

  interface Props{
      children : any;
      ContainerStyle? : ViewStyle  | ViewStyle[]
  }
  const Container = (props : Props)=>{
    return(
      
          <SafeAreaView style={[style.container , props.ContainerStyle]}> 
            {props.children}
          </SafeAreaView>
         
          )
      }
      const style = StyleSheet.create({
        container:{
          flex: 1,
          flexDirection: "column",
          justifyContent: 'center',
          alignItems:"stretch", 
           // backgroundColor:PrimaryTheme.$BACK_GRIOND
        },
    })
export default Container;



