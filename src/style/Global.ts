
import React from 'react'
import { widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { PrimaryTheme } from '../style/Theme';

export const Typography : any = {
    title:{
     fontSize:wp('9%'),
     color:PrimaryTheme.$TEXT_COLOR_900,
     fontWeight:"600",
     fontFamily:'Muli',
     marginBottom: wp("2%"),
    },
    heading:{
        fontSize:wp('8%'),
        color:PrimaryTheme.$TEXT_COLOR_900,
        fontWeight:"bold"
    },
    subheading:{
        fontSize:wp('5%'),
        color:PrimaryTheme.$TEXT_COLOR_700,
        fontFamily:'Muli',
        fontWeight:"500"
    },
    paragraph:{
        fontSize:wp('4%'),
        color:PrimaryTheme.$TEXT_COLOR_300,
        fontFamily:'Muli',
    },
    lighText:{
        fontSize:wp('3.2%'),
        color:PrimaryTheme.$TEXT_COLOR_500,
        fontFamily:'Muli',
    },
    errorText:{
        fontSize:wp('3.2%'),
        color:PrimaryTheme.$Red_COLOR,
        fontFamily:'Muli',
    }
}


export const Spaning = {
    tiny:{
      marginLeft: wp("0.25%"),
      marginRight:wp("0.25%"),
      marginTop: hp("0.25%"),
      marginBottom: hp("0.25%"),
    },
    small:{
        marginLeft: wp("0.5%"),
        marginRight:wp("0.5%"),
        marginTop: hp("0.5%"),
        marginBottom: hp("0.5%"),
    },
    regural:{
        marginLeft: wp("1%"),
        marginRight:wp("1%"),
        marginTop: hp("1%"),
        marginBottom: hp("1%"),
    },
    larg:{
        marginLeft: wp("1.5%"),
        marginRight:wp("1.5%"),
        marginTop: hp("1.5%"),
        marginBottom: hp("1.5%"),
    },
    extrlarg:{
        marginLeft: wp("2%"),
        marginRight:wp("2%"),
        marginTop: hp("2%"),
        marginBottom: hp("2%"),
    },

}

export const pComponentStyle = {
    textinput:{
        borderWidth:3,
        width: wp('75%'),
        marginBottom:20,
        borderRadius:5,
        fontSize:20,
        borderColor:"#fff",
        backgroundColor: "#fff",
        },
} 
export const  lComponentStyle = {
    textinput:{
        ...pComponentStyle.textinput, width: wp('100%'), 
         },
} 


