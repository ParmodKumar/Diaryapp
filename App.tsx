import * as React from 'react';
import {Navigator} from './src/utills/navigation/Navigtor'
import Reactotron from 'reactotron-react-native';
    interface Props{
        }
    interface State{  
    }
    export class App extends React.Component<Props,State>{
        constructor(props) {
          super(props);
          this.configReactotron();
      }
      configReactotron(){
        Reactotron.clear();
        return Reactotron.configure({
          host:'192.168.1.227',
          port:9090,
          name:"ReactNativeCourse"
        }).connect();
      }
      render(){
        return <Navigator />
      }
    }
